import fetch from "node-fetch";

const getFile = async (url, blob) => {
      return await fetch(url)
        .then((res) => {
          if (blob) {
            return res.blob();
          } else {
            return res.text();
          }
        })
        .then((body) => body);
    }

export default getFile;