import webshot from "node-webshot";

export default async function handler(req, res) {
  let data = "data:" + "image/png" + ";base64,";
  const url = JSON.parse(req.body).url;
  // const url = 'google.com';
  webshot(url, null, {
      windowSize: {
        width: 600,
        height: 600
      },
      renderDelay:1000,
      captureSelector: "#react-app",
    })
    .on('data', d => { 
      data += d.toString('base64');
    }).on('end', () => {
      res.status(200).json({ data })
    });
}